# GraphQL | Resolvers

----

> References:
>
> - [https://typegraphql.com/docs/generic-types.html](https://typegraphql.com/docs/generic-types.html)
> - [https://typegraphql.com/docs/resolvers.html](https://typegraphql.com/docs/resolvers.html)
> - [https://typegraphql.com/docs/inheritance.html#resolvers-inheritance](https://typegraphql.com/docs/inheritance.html#resolvers-inheritance)



Resolvers trong GraphQL cũng giống như Controllers trong RestAPI

Ta định nghĩa 1 resolver cũng giống như định nghĩa 1 function cho 1 endpoint cụ thể



## 1. Tạo resolvers

> https://typegraphql.com/docs/resolvers.html

Cái Resolvers sẽ nằm trong thư mục `src/api/resolvers/*.ts`

Example:

```typescript
@Injectable()
@Resolver(() => Farm)
abstract class FarmResolver {
    public readonly repository: Repository<Farm>;

    protected constructor() {
        this.repository = getRepository(Farm);
    }

    @Query(() => PaginateResponse, { name: "farms" })
    public async findAll(@Args() query: ExtendedQuery): Promise<PaginateResponse> {
        const parsedQuery = this.parseHelper.fullQueryParam(query) as ICrudOption;
        const [list, count] = await ConditionQueryBuilder.ofRepository(this.repository)
            .select(undefined)
            .relations([])
            .take(parsedQuery.take)
            .skip(parsedQuery.skip)
            .condition(parsedQuery.where)
            .order(query.order)
            .search(query.search)
            .build()
            .getManyAndCount();
        return new PaginateResponse(query, { list, count });
    }

    @Query(() => TClass, { name: "farm" })
    public async findOne(@Arg('id') id: string): Promise<T> {
        return await this.repository.findOne(id);
    }
}
```

Thì schema sẽ được generate ra là:

```gql
query {
	farms(...): PaginateReponse<Farm>
    farm(id String!): Farm
    # ...
}
```



## 2. Kế thừa và đa hình

Trường hợp có nhiều resolver giống nhau về cái hàm bên trong, để tránh duplicate code và thuận tiện, ta có thể tạo 1 Generic Resolver và cái Resolver khác có thể kế thừa lại

Chi tiết:  

- [https://typegraphql.com/docs/inheritance.html#resolvers-inheritance](https://typegraphql.com/docs/inheritance.html#resolvers-inheritance)
- [https://typegraphql.com/docs/generic-types.html](https://typegraphql.com/docs/generic-types.html)

Example:

```typescript
// src/api/resolvers/BaseResolver
export function BaseResolver<T>(TClassFunc: () => ClassType<T>, name: string) {
    const TClass = TClassFunc();

    @ObjectType(`${TClass.name}PaginateResponse`)
    class PaginateResponse extends GraphQLPaginationResponse(TClass) {}

    @Injectable()
    @Resolver({ isAbstract: true })
    abstract class AbstractResolver {
        public readonly repository: Repository<T>;

        protected constructor(
            public readonly parseHelper: ParseHelper
        ) {
            this.repository = getRepository(TClass);
        }

        @Query(() => PaginateResponse, { name: plural(name) })
        public async findAll(@Args() query: ExtendedQuery): Promise<PaginateResponse> {
            const parsedQuery = this.parseHelper.fullQueryParam(query) as ICrudOption;
            const [list, count] = await ConditionQueryBuilder.ofRepository(this.repository)
                .select(undefined)
                .relations([])
                .take(parsedQuery.take)
                .skip(parsedQuery.skip)
                .condition(parsedQuery.where)
                .order(query.order)
                .search(query.search)
                .build()
                .getManyAndCount();
            return new PaginateResponse(query, { list, count });
        }

        @Query(() => TClass, { name })
        public async findOne(@Arg('id') id: string): Promise<T> {
            return await this.repository.findOne(id);
        }
    }

    return AbstractResolver;
}
```



```typescript
// file src/api/resolvers/FarmResolver
import { Resolver } from 'type-graphql';
import { Farm } from '../models';
import { BaseResolver } from './BaseResolver';

@Resolver()
export class FarmResolver extends BaseResolver(() => Farm, 'farm') {}

// file src/api/resolvers/AreaResolver
import { Area } from '../models';
import { BaseResolver } from './BaseResolver';
import { Resolver } from 'type-graphql';

@Resolver()
export class AreaResolver extends BaseResolver(() => Area, 'area') {}
```



Kết quả ta được

```gql
type Query {
	farms(...): Farm[]
	farm(id String!): Farm
	areas(...): Area[]
	area(id String!): Area
	# ...
}
```

