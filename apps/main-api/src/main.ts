import { NestFactory } from "@nestjs/core";
import { ValidationPipe } from "@nestjs/common";
import * as winston from "winston";
import { WINSTON_MODULE_NEST_PROVIDER } from "nest-winston";

import { AppModule } from "./app.module";
import { useSwagger } from "./loaders/swaggerLoader";

import { env } from "@app/common/env";

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
    app.enableCors();
    app.setGlobalPrefix(env.app.routePrefix);
    app.useGlobalPipes(new ValidationPipe({ transform: true, forbidUnknownValues: true }));
    useSwagger(app);
    await app.listen(+env.app.port || 4000);
}

bootstrap().catch((err) => {
    winston.error(err);
    console.error(err);
});
