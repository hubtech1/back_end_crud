import { Injectable } from "@nestjs/common";

import { env } from "@app/common/env";

@Injectable()
export class AppService {
    getHello(): Object {
        return {
            name: env.app.name,
            version: env.app.version,
            description: env.app.description,
        };
    }
}
