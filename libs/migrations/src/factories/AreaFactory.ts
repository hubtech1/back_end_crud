import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";

import { Area } from "@app/entity-service/modules/general/entities";
import { CommonUtil } from "@app/common";

define(
    Area,
    (
        faker: typeof Faker,
    ) => {
        const area = new Area();

        area.id = uuid.v1();
        area.locationID = uuid.v1();
        area.ownerID = uuid.v1();
        area.shortID = CommonUtil.genShortID();
        area.code = faker.lorem.word();
        area.avatar = area.avatarThumbnail = faker.lorem.lines();
        area.totalArea = faker.random.number({ min: 50, max: 100 });
        area.totalQuantity = faker.random.number({ min: 50, max: 100 });
        area.name = faker.lorem.word();
        area.type = faker.random.number(5);
        area.description = faker.lorem.paragraphs(4);
        area.address = faker.address.secondaryAddress();
        area.latitude = faker.address.latitude();
        area.longitude = faker.address.longitude();

        return area;
    },
);
