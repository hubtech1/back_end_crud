import { forwardRef, MiddlewareConsumer, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { Area, Staff, TimeSheet } from "./entities";
import { AreaRepository, StaffRepository, TimeSheetRepository } from "./repositories";
import { AreaService, StaffService, TimeSheetService } from "./services";
import { AreaController, StaffController, TimeSheetController } from "./controllers";

import { CommonModule } from "@app/common";

const SERVICES = [AreaService, StaffService, TimeSheetService];

@Module({
    imports: [TypeOrmModule.forFeature([Area, AreaRepository, Staff, StaffRepository, TimeSheet, TimeSheetRepository]), forwardRef(() => CommonModule)],
    providers: [...SERVICES],
    exports: [...SERVICES, TypeOrmModule],
    controllers: [AreaController, StaffController, TimeSheetController],
})
export class GeneralModule {
    configure(consumer: MiddlewareConsumer) {
    }
}
