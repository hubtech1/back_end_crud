import { EntityRepository, Repository } from "typeorm";

import { Area } from "../entities";

@EntityRepository(Area)
export class AreaRepository extends Repository<Area> {

}
