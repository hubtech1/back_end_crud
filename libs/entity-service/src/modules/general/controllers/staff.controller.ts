import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, UseInterceptors } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";


import { StaffRepository } from "../repositories";
import { StaffNotFoundError } from "../errors";
import { StaffService } from "../services";

import {
    ParseUtil,
    ListDataDto,
    BaseQueryDto,
    FullQueryDto,
    ErrorResponseDto,
    ListResponseInterceptor,
    OnUndefinedInterceptor,
    WhereQueryDto,
} from "@app/common";
import { CreateStaffBodyDto, StaffResponseDto, UpdateStaffBodyDto } from "../dto";
import { Staff } from "../entities";
import { Authorized } from "type-graphql";
import { IndexMetadata } from "typeorm/metadata/IndexMetadata";


@ApiTags("staffs")
@Controller("/staffs")
@ApiResponse({ type: ErrorResponseDto, status: 401 })
@ApiResponse({ type: ErrorResponseDto, status: 404 })
@ApiResponse({ type: ErrorResponseDto, status: 500 })
export class StaffController {
    constructor(
        private staffService: StaffService,
        private parseUtil: ParseUtil,
        private staffRepository: StaffRepository,

    ) { }

    // Lấy hết danh sách Staff
    @Get()
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated', 
        `
    })
    @UseInterceptors(ListResponseInterceptor)
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, isArray: true, status: 200 })
    public find(
        @Query() query: FullQueryDto,

    ): Promise<ListDataDto<Staff> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query)
        return this.staffService.find(queryParse)
    }

    // Tạo thêm Staff
    // @Authorized()
    @Post()
    @ApiOperation({
        description: `
        role: 0: 'staff', 1: 'manager', 2: 'director'`,
    })
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, status: 200 })
    public async create(
        @Body() body: CreateStaffBodyDto,
    ): Promise<Staff | undefined> {
        return this.staffService.create(body)
    }

    // Tìm 1 staff theo id
    @Get("/:id")
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated',
        role: 0: 'staff', 1: 'manager', 2: 'director', 
        `
    })
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, status: 200 })
    public async findOne(
        @Param("id") id: string,
        @Query() query: BaseQueryDto,
    ): Promise<Staff | undefined> {
        const queryParse = this.parseUtil.baseQueryParam(query);
        return this.staffService.findOne(id, queryParse)
    }

    // Update thông tin nhân viên

    @Authorized()
    @Patch("/:id")
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, status: 200 })
    public async update(
        @Param("id") id: string,
        @Body() body: UpdateStaffBodyDto
    ): Promise<Staff | undefined> {
        const bodyParse = this.parseUtil.removeUndefinedProperty(body);
        return this.staffService.update(id, bodyParse)
    }

    // Xóa nhân viên
    @Authorized()
    @Delete("/:id")
    @OnUndefinedInterceptor(StaffNotFoundError)
    public async delete(
        @Param("id") id: string,
    ): Promise<Staff | undefined> {
        return this.staffService.delete(id)
    }













}