import { Column, Entity as EntityTypeORM } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "@app/common/entities";


@EntityTypeORM()
@ObjectType()
export class Department extends Base {
    @Column({ name: "department_code", nullable: false })
    @Field()
    public departmentCode: number;

    @Column({ name: "department_name", nullable: false })
    @Field()
    public departmentName: string;

}