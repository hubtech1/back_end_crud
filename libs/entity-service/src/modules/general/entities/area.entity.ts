import { Column, Entity as EntityTypeORM } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "@app/common/entities";

@EntityTypeORM()
@ObjectType()
export class Area extends Base {
    @Column({ name: "location_id", nullable: true })
    @Field()
    public locationID: string;

    @Column({ name: "owner_id", nullable: false })
    @Field()
    public ownerID: string;

    @Column({ name: "entity_id", nullable: true })
    @Field()
    public entityID: string;

    @Column({ name: "short_id", unique: true, nullable: false })
    public shortID: string;

    @Column({ nullable: false })
    @Field()
    public code: string;

    @Column({ name: "total_area", nullable: true, default: 0 })
    @Field()
    public totalArea: number;

    @Column({ name: "total_quantity", nullable: true, default: 0 })
    @Field()
    public totalQuantity: number;

    @Column({ nullable: false })
    @Field()
    public avatar: string;

    @Column({ name: "avatar_thumbnail", nullable: true })
    @Field()
    public avatarThumbnail: string;

    @Column({ name: "total_product_object", nullable: false, default: 0 })
    @Field()
    public totalProductObject: number;

    @Column({ nullable: false })
    @Field()
    public name: string;

    // 0: 'field area', 1: 'farming area', 2: 'production area', 3: 'field & product area', 4: 'field & production area', 5: 'others'
    @Column({ nullable: false, default: 0 })
    @Field({
        description:
            "0: field area, 1: farming area, 2: production area, 3: field & product area, 4: field & production area, 5: others",
    })
    public type: number;

    @Column({ nullable: true })
    @Field()
    public description: string;

    @Column({ nullable: true })
    @Field()
    public content: string;

    @Column({ nullable: true })
    @Field()
    public address: string;

    @Column({ nullable: true })
    @Field()
    public latitude: string;

    @Column({ nullable: true })
    @Field()
    public longitude: string;
}
