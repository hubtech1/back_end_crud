import _ from "lodash";
import * as uuid from "uuid";
import { Injectable } from "@nestjs/common";

import { StaffRepository } from "../repositories";

import { DBUtil, Logger, ListDataDto, CrudOption, env, CommonUtil } from "@app/common";
import { CreateStaffBodyDto, UpdateStaffBodyDto } from "@app/entity-service/modules/general/dto";
import { Area, Staff } from "../entities";


@Injectable()
export class StaffService {
    constructor(
        private staffRepository: StaffRepository,
        private dbUtil: DBUtil,
        private commonUtil: CommonUtil,
        private logger: Logger
    ) { }

    public async find(option: CrudOption = {}): Promise<ListDataDto<Staff> | undefined> {
        this.logger.info("Find all Staff");
        return this.dbUtil.findAndCount(this.staffRepository, option);
    }

    public async create(
        body: CreateStaffBodyDto,
        option: CrudOption = {},
    ): Promise<Staff> {
        this.logger.info("Create a new Staff");

        const staff = new Staff;
        _.assign(staff, body);
        staff.id = uuid.v1()
        staff.avatar = staff.avatar ?? env.defaultValue.defaultAvatar;

        return this.staffRepository.save(staff)
    }

    public async findOne(id: string, option: CrudOption = {}): Promise<Staff> {
        this.logger.info("Find one Staff");

        const result = await this.dbUtil.findOne(this.staffRepository, id, option);

        if (!result) {
            return result
        }

        return result
    }

    public async update(id: string, body: UpdateStaffBodyDto, option?: CrudOption): Promise<Staff | undefined> {
        this.logger.info("Update some fields a Staff");

        await this.staffRepository.update(id, body);

        return this.staffRepository.findOne(id)
    }

    public async delete(id: string, option?: CrudOption): Promise<Staff | undefined> {
        this.logger.info("Delete a Staff");
        const item = await this.staffRepository.findOne(id);
        if (item === undefined) {
            return undefined
        } else {
            await this.staffRepository.delete(id)
        }
        return item
    }
}