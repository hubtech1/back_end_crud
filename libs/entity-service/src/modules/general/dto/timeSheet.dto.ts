import { IsDateString, IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString, IsUrl, IsUUID, Length, Max, Min } from "class-validator";
import { Exclude, Expose } from "class-transformer";
import { OmitType } from "@nestjs/swagger";



@Exclude()
export class BaseTimeSheetDto {
    @Expose()
    @IsNotEmpty()
    @IsString()
    public staffId: string

    @Expose()
    @IsNotEmpty()
    @IsString()
    public dateId: string;

    @Expose()
    @IsNotEmpty()
    public startTime: string;

    @Expose()
    @IsNotEmpty()
    @IsNumber()
    public timeSpent: number;

    @Expose()
    public title: string;

    @Expose()
    public description: string;

    @Expose()
    public attach: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;

    @Expose()
    @IsDateString()
    @IsOptional()
    public createdAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public updatedAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public deletedAt: string;



}

@Exclude()
export class TimeSheetResponseDto extends BaseTimeSheetDto {
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public id: string;
}

export class CreateTimeSheetBodyDto extends OmitType(BaseTimeSheetDto, [
    "status",
    "createdAt",
    "updatedAt",
    "deletedAt",
] as const) { }

@Exclude()
export class UpdateTimeSheetBodyDto {
    @Expose()
    @IsNotEmpty()
    @IsString()
    public staffId: string

    @Expose()
    @IsNotEmpty()
    @IsString()
    public dateId: string;

    @Expose()
    @IsNotEmpty()
    public startTime: string;

    @Expose()
    @IsNotEmpty()
    @IsNumber()
    public timeSpent: number;

    @Expose()
    public title: string;

    @Expose()
    public description: string;

    @Expose()
    public attach: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;

}


