import { Injectable } from "@nestjs/common";

import { Logger } from "../lib";

const DEFAULT_ID = "123456789";
const ANONYMOUS_TYPE = "ANONYMOUS_TYPE";

@Injectable()
export class GenerateImageService {
    constructor(private logger: Logger) {}

    public async generateImageFromText(text: string, config?: any): Promise<string | undefined> {
        this.logger.info("Generate image from text");

        const { background, size, fontSize, isBold } = config;
        return `https://ui-avatars.com/api/?background=${background}&color=fff&name=${text}&size=${size}&font-size=${fontSize}&bold=${isBold}`;

        // const { id, type } = options;
        // const base64 = await textToImage.generate(text, config);
        //
        // try {
        //     const urlBase64 = "base64".split(",", 2);
        //     const formData = new FormData();
        //     formData.append("image.png", urlBase64[1]);
        //
        //     const response = await axios.create({ baseURL: env.farmhub.uploadService })({
        //         url: "/cdn",
        //         headers: {
        //             "Api-Key": env.defaultValue.internalAPIkey,
        //             "Content-Type": `multipart/form-data; boundary=${formData.getBoundary()}`,
        //         },
        //         params: {
        //             id: id ?? DEFAULT_ID,
        //             type: type ?? ANONYMOUS_TYPE,
        //         },
        //         method: "POST",
        //         data: formData,
        //     });
        //
        //     const { msg } = response.data;
        //     const { url } = msg[0];
        //
        //     return `https://ui-avatars.com/api/?background=${bgColor}&color=${textColor}&name=${text}&size=${customHeight}&font-size=0.6&bold=true`;
        // } catch (err: any) {
        //     if (err.response) {
        //         this.logger.error(`[Upload Service] ${err.response?.status} ${err.response?.data?.message}`);
        //         throw new HttpException(err.response?.data?.message + " at UploadService", 500);
        //     } else {
        //         throw err;
        //     }
        // }
    }

    public static async generateImageFromText(text: string, config?: any): Promise<string | undefined> {
        const { background, size, fontSize, isBold } = config;
        return `https://ui-avatars.com/api/?background=${background}&color=fff&name=${text}&size=${size}&font-size=${fontSize}&bold=${isBold}`;
    }
}
