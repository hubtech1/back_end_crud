import axios from "axios";
import { HttpException, Injectable } from "@nestjs/common";

import { env } from "../env";
import { Logger } from "../lib";

import { Notify } from "@app/common/interfaces";

/*
- activity_type=0: Da mua hang cua ban
- activity_type=10: Gui loi moi lien ket
- activity_type=11: Chap nhan loi moi lien ket
- activity_type=12: Chap nhan loi moi dong bo san pham
- activity_type=20: Thuc hien chien dich
- activity_type=30: Cap nhat trang thai don hang
- activity_type=31: Tron don hang
- activity_type=32: Chuyen tiep don hang
- activity_type=33: Cap nhat thuc giao va chuyen sang trang thai giao hang
- activity_type=34: Cap nhat thuc nhan va chuyen sang trang thai cho KH xac nhan
- activity_type=40: Yeu thich san pham
- activity_type=41: Danh gia san pham
- activity_type=42: Danh gia
- activity_type=43: Danh gia doi tuong nuoi trong
- activity_type=44: Moi ban tham gia nhom mua chung
- activity_type=45: Moi ban tham gia chien dich
- activity_type=46: Bat dau theo doi
*/

/*
- dst_type=0: order
- dst_type=1: invitation
- dst_type=2: partner
- dst_type=3: entity
- dst_type=4: product
- dst_type=5: co-buy
- dst_type=6: campaign
*/

@Injectable()
export class PushLogService {
    constructor(private logger: Logger) {}

    public async sendNotify(notifies: Notify[]): Promise<void | undefined> {
        this.logger.info("Send notify to push log service");

        try {
            console.log({ notifies });

            notifies.length !== 0 &&
                (await axios.create({ baseURL: env.hubtech.pushLogService })({
                    url: "/noti",
                    headers: { "Api-Key": env.defaultValue.internalAPIkey },
                    method: "POST",
                    data: notifies,
                }));

            return;
        } catch (err: any) {
            if (err.response) {
                this.logger.error(`[PushLog Service] ${err.response?.status} ${err.response?.data?.message}`);
                throw new HttpException(err.response?.data?.message + " at PushLogService", 500);
            } else {
                throw err;
            }
        }
    }
}
