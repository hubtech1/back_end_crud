import { Injectable } from "@nestjs/common";
import axios from "axios";

import { Logger, env } from "@app/common";

@Injectable()
export class IdentityService {
    constructor(private logger: Logger) {}

    public async createUser(entity: any, token: string): Promise<any> {
        this.logger.info("[Identity Service V2] Create a new user");

        return axios.create({ baseURL: env.hubtech.identityServiceV2 })({
            url: "/users",
            headers: { "Token-ID": token ?? "is-debug", "Api-Key": env.defaultValue.internalAPIkey },
            method: "POST",
            data: {
                id: entity.id,
                fullName: entity.fullName,
                displayName: entity.displayName,
                email: entity.email,
                phoneNumber: entity.phoneNumber,
                avatar: entity.avatar,
                password: entity.password,
            },
        });
    }

    public async deleteUser(id: string): Promise<any> {
        this.logger.info("[Identity Service V2] Delete a user");

        return axios.create({ baseURL: env.hubtech.identityServiceV2 })({
            url: `/users/${id}`,
            headers: { "Api-Key": env.defaultValue.internalAPIkey },
            method: "DELETE",
        });
    }

    public async createEntity(entity: any): Promise<any> {
        this.logger.info("[Identity Service V2] Create a new entity");

        return axios.create({ baseURL: env.hubtech.identityServiceV2 })({
            url: "/entities",
            headers: { "Api-Key": env.defaultValue.internalAPIkey },
            method: "POST",
            data: {
                id: entity.id,
                fullName: entity.fullName,
                displayName: entity.displayName,
                avatar: entity.avatar,
                creatorID: entity.creatorID,
                type: entity.type,
            },
        });
    }

    public async deleteEntity(id: string): Promise<any> {
        this.logger.info("[Identity Service V2] Delete a delete");

        return axios.create({ baseURL: env.hubtech.identityServiceV2 })({
            url: `/entities/${id}`,
            headers: { "Api-Key": env.defaultValue.internalAPIkey },
            method: "DELETE",
        });
    }

    public async createEmployee(id: string, user: any, token: string): Promise<any> {
        this.logger.info("[Identity Service V2] Create a new employee");

        return axios.create({ baseURL: env.hubtech.identityServiceV2 })({
            url: `/entities/${id}/members`,
            headers: { authorization: token },
            method: "POST",
            data: {
                userID: user.id,
                role: user.role,
                permission: user.permission,
            },
        });
    }
}
