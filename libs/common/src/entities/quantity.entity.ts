import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class Quantity {
    @Field((type) => Int)
    public few_product: number;

    @Field((type) => Int)
    public have_product: number;
}
