import { Injectable } from "@nestjs/common";

import { BaseQueryDto, FullQueryDto } from "../dto/query.dto";

@Injectable()
export class ParseUtil {
    public fullQueryParam(query: FullQueryDto): any {
        const baseQuery = this.baseQueryParam(query);

        if (typeof baseQuery.order === "string") {
            baseQuery.order = JSON.parse(baseQuery.order);
        }

        // default skip follow (page-1) * take
        if (baseQuery.page && baseQuery.take) {
            baseQuery.skip = (baseQuery.page - 1) * baseQuery.take;
        }

        // if page, take, skip = undefined, default skip = 0
        if (!baseQuery.skip) {
            baseQuery.skip = 0;
        }

        return baseQuery;
    }

    public baseQueryParam(query: BaseQueryDto): any {
        if (query.where) {
            query.where = JSON.parse(query.where);
        }

        if (query.flatten) {
            query.flatten = JSON.parse(query.flatten);
        }

        if (query.select) {
            query.select = JSON.parse(query.select);
        }

        if (query.relations) {
            query.relations = JSON.parse(query.relations);
        }

        return query;
    }

    public whereQueryParam(query: BaseQueryDto): any {
        if (query.where) {
            query.where = JSON.parse(query.where);
        }

        return query;
    }

    public removeUndefinedProperty(obj: object): any {
        const cloneObj = { ...obj };
        Object.keys(cloneObj).forEach((key) => cloneObj[key] === undefined && delete cloneObj[key]);

        return cloneObj;
    }

    public removeUndefinedPropertyForArray(arr: object[]): any {
        return arr.map((item) => this.removeUndefinedProperty(item));
    }
}
