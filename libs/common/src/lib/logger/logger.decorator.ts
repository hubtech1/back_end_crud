import { Inject } from "@nestjs/common";

export const LOGGER_TOKEN = "LoggerInterface";

export function InjectLogger(scope: string) {
    return Inject("LoggerInterface");
}
