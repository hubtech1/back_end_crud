export interface CrudOption {
    page?: number;
    take?: number;
    skip?: number;
    type?: number;
    mode?: number;
    order?: any;
    select?: any;
    where?: any;
    flatten?: any;
    relations?: any;
    cache?: boolean | number;
}

export interface Notify {
    userID: string;
    entityID?: string;
    src: SRCNotify;
    activityType: number;
    dst: DSTNotify;
    dstType: number;
}

export interface SRCNotify {
    id: string;
    name: string;
    avatar: string;
}

export interface DSTNotify {
    id: string;
    name: string;
}
